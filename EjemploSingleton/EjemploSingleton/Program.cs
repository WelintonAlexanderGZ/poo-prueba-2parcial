﻿using System;

namespace EjemploSingleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //No se puedo obtener la instancia directamente
            //eSingleton single = new eSingleton();

            //Obtenemos la instancia, se crea por primera vez
            eSingleton producto1 = eSingleton.GetInstancia();

            //Realizamos algo con la instancia 
            producto1.ColocarDatos("Detergente de 5 Kilos", 5);
            producto1.Descuento();
            Console.WriteLine(producto1);
            Console.WriteLine("-----------------------------------------------------------");

            //Obtenemos la instancia 
            eSingleton producto2 = eSingleton.GetInstancia();

            //Comprobamos que es la misma instancia
            //Si la es, tendrea el mismo estado
            Console.WriteLine(producto2);
        }
    }
}
