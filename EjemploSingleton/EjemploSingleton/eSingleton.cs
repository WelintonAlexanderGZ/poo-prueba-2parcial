﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjemploSingleton
{
    class eSingleton
    {
        // Guardamos la unica intancia que va a existir 
        private static eSingleton Instancia;

        //Datos propios de la clase
        private string Producto;
        private int Precio;

        //Creamos el constructor privado
        private eSingleton() 
        {
            Producto = "No asignado";
            Precio = 000;
             
        }
        public static eSingleton GetInstancia()
        {
            //Verificamos si no existe la instancia 
            if (Instancia==null)
            {
                //Sino existe, instanciamos
                Instancia = new eSingleton();
                Console.WriteLine("-----INSTANCIA CREADA POR PRIMERA VEZ-----");
            }
            //Retornamos la instancia
            return Instancia;
        }

        //Metodos propios de la clase 
        public override string ToString()
        {
            return string.Format ("El producto {0}, cuenta con un precio de {1}$",Producto,Precio);
        }
         public void ColocarDatos (String pProducto, int pPrecio)
        {
            Producto = pProducto;
            Precio = pPrecio;
        }

        //Metodo que nos permite asignar si un producto tendra o no descuento
        public void Descuento()
        {
            if (Precio>=20)
            {
                Console.WriteLine("El producto {0} obtiene un descuento", Producto);
            }
            else
            {
                Console.WriteLine("El producto {0} no obtiene un descuento", Producto);
            }
            
        }          
    }
}